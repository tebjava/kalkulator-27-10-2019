import java.util.Scanner;

public class Kalkulator {
	/*
	 * Funkcjonalnosc do stworzenia:
	 * - modulo (to reszta z dzielnia, wraz z warunkami)
	 * - pierwiastki dowolnego stopnia (Math.sqrt zapewnia jedynie stopien 2)
	 * - funkcje trygonometryczne (sprawdzic jak dzialaja stworzone do tego funkcje w klasie Math)
	 * Opcjonalnie zapetlic program (by dopiero podanie liczby konczacej program wychodzil z kosoli)
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Kalkulator w Java");
		System.out.println("Wybierz jedna z operacji:");
		System.out.println("1. Dodawanie\n2. Odejmowanie"
				+ "\n3. Mnożenie\n4. Dzielenie"
				+ "\n5. Potegowanie\n6. Pierwiastkowanie"
				+ "\n7. Silnia\n8. Wyjscie");
		System.out.print("Wybor: ");
		Scanner skan=new Scanner(System.in);
		int wybor=skan.nextInt();
		
		if(wybor==1) {
			System.out.print("Podaj wartosc a: ");
			int a = skan.nextInt();
			System.out.print("Podaj wartosc b: ");
			int b = skan.nextInt();
			System.out.println(a + " + " + b + " = " + (a+b));
		}
		
		if(wybor==2) {
			System.out.print("Podaj wartosc a: ");
			int a = skan.nextInt();
			System.out.print("Podaj wartosc b: ");
			int b = skan.nextInt();
			System.out.println(a + " - " + b + " = " + (a-b));
		}
		
		if(wybor==3) {
			System.out.print("Podaj wartosc a: ");
			int a = skan.nextInt();
			System.out.print("Podaj wartosc b: ");
			int b = skan.nextInt();
			System.out.println(a + " * " + b + " = " + (a*b));
		}
		
		if(wybor==4) {
			System.out.print("Podaj wartosc a: ");
			int a = skan.nextInt();
			System.out.print("Podaj wartosc b: ");
			int b = skan.nextInt();
			//warunek konieczny by probgram nie zakonczyl sie 
			//bledem - nie mozna dzielic przez 0 
			if (b!=0) {		
				//poniewaz dzielimy dwie wartosci calkowite
				//Java poda nam wynik calkowity, wyswietlajac
				//czesc calosci i pomijajac czesc ulamkowa
				//System.out.println(a + " / " + b + " = " + (a/b));
				//rozwiazaniem tego jest przemnozenie jeden z 
				//wartosci dzielenia przez liczbe zmiennoprzecinkowa
				//czyli
				//System.out.println(a + " / " + b + " = " + (a/(b*1.)));
				//lub
				System.out.println(a + " / " + b + " = " + (a*1./b));
			}
			//gdyby jednak ktos probowal dzielic przez 0...
			else {
				System.out.println("Nie mozna dzielic przez 0!");
			}
		}
		
		if(wybor==5) {
			System.out.print("Podaj podstawe: ");
			int a = skan.nextInt();
			System.out.print("potege: ");
			int b = skan.nextInt();
			//2^3 = 2*2*2 = 8
			//12^2 = 12*12 = 144
			//4^4 = 4*4*4*4 = 16*16 = 256
			System.out.println(a + "^" + b + " = " + Math.pow(a, b));
		}
		
		if(wybor==7) {
			System.out.print("Podaj liczbe: ");
			int a = skan.nextInt();
			//Silnia (factorial) - wynikiem jest wymnożenie
			//wszystkich liczb składajcych sie na nasza liczbe
			//Przyklady:
			//4! = 1*2*3*4 = 24
			//5! = 1*2*3*4*5 = 120
			//3! = 1*2*3 = 6
			//Generalnie nie mozemy okreslic jaka liczbe poda
			//uzytkownik, a co za tym idzie ile razy bedziemy
			//musieli wykonac mnozenia
			
			/*Petle w programowaniu
			 * Petle pozwalaja na wykonywanie okreslonego kodu
			 * do czasu az warunek podany na ich wejscu bedzie
			 * spelniany. Tym samym petle sa podobne do warunkow
			 * jednak warunki wykonuja sie dokladnie raz.
			 *  
			 * Pierwsza z podstawowych petli jest petla for.
			 * Programista podaje w niej kolejno:
			 * - zmienna (badz tworzy zmienna), ktora zawiera
			 *   wartosc, dla ktorej petla startuje
			 * - warunek, ktory musi byc spelniony by kod zawarty w petli
			 *   sie wykonywal
			 * - instrukcje jezykowa, ktora ma sie wykonac po kazdorazowym
			 *   wykonaniu petli
			 *   
			 * Wyglad petli for:
			 * 
			 * for(zmienna;warunek;instrukcja) {
			 * 		//kod do wykonania
			 * }
			 * 
			 * Przyklad petli for:
			 * 
			 * for(int i=0;i<10;i=i+1) {
			 * 		System.out.println("Odliczam " + i);
			 * }
			 * 
			 * Powyższa petla wyswietli nam 10 linii tekstu
			 * Odliczam 0
			 * Odliczam 1
			 * Odliczam 2
			 * Odliczam 3
			 * Odliczam 4
			 * Odliczam 5
			 * Odliczam 6
			 * Odliczam 7
			 * Odliczam 8
			 * Odliczam 9
			 * 
			 * Kolejna petla, bardziej uniwersalna jest while.
			 * Jak sama jest nazwa okresla, wykonuje do czasu
			 * trwania okreslonego warunku.
			 * 
			 * Skladnia:
			 * 
			 * while(warunek) {
			 * 		//wykonywany kod
			 * }
			 * 
			 * W przeciwienswie do for while musi posiadac
			 * wewnatrz wykonywanego kodu modyfikator warunku
			 * poniewaz w przeciwnym wypadku bedzie sie
			 * wykonywal w nieskonczonosc!
			 * 
			 * Przyklady dzialania while:
			 * a) odpowiednik for z poprzedniego przykladu:
			 * int i = 0;
			 * while(i<10) {
			 * 		System.out.println("Odliczam " + i);
			 * 		i = i+1;
			 * }
			 * b) przyklad na zapetlenie programu (az uzytkownik nie przerwie dzialania):
			 * int wybor=0;
			 * while(wybor!=5) {
			 * 		Scanner s = new Scanner(System.in);
			 * 		System.out.print("Podaj wybor: ");
			 * 		//podanie przez uzytkownika 5 spowoduje zakonczenie petli
			 * 		wybor=s.nextInt();
			 * }
			 * 
			 * Ostatnia z podstawowych petli jest do..while();
			 * Jej cialo (deklaracja + definicja):
			 * 
			 * do {
			 * 		//instrukcje do wykonania
			 * } while(warunek);
			 * 
			 * Dziala tak samo jak while JEDNAK JEJ KOD
			 * ZAWSZE WYKONA SIE CO NAJMNIEJ RAZ (nawet
			 * jeżeli warunek nie zostanie spelniony)
			 * 
			 * Przyklady:
			 * a) odliczanie jak przy for
			 * int i = 0;
			 * do {
			 * 		System.out.println("Odliczam " + i);
			 * 		i = i+1;
			 * }while(i<10)
			 * 
			 * b) blad w wykorzystaniu do..while:
			 * int wybor=5;
			 * do {
			 * 		Scanner s = new Scanner(System.in);
			 * 		System.out.print("Podaj wybor: ");
			 * 		//podanie przez uzytkownika 5 spowoduje zakonczenie petli
			 * 		wybor=s.nextInt();	
			 * } while(wybor!=5);
			 * 
			 * Powyższy przyklad nie powinien sie, wedle
			 * zalozen, w ogole wykonac, jednak petla
			 * najpierw interpretuje kod a dopiero po jego
			 * wykonaniu sprawdza warunek, ktory poczatkow
			 * byl niespelniony (moze ulec zmianie podczas
			 * wykonania)
			 */
			int c = 1;
			//jezeli uzytkownik poda wartosc 0 - petla sie nie wykona (wartosc c i tak jest 1)
			//jezeli poda wartosc 1 - w zasadzie nie musi sie wykonywac (ale sie wykona)
			//celem optymalizacji mozna by bylo rozpoczac petle z i=2
			for(int i=1;i<=a;i=i+1) {
				c=c*i;
			}
			System.out.println(a + "! = " + c);
		}
		
		if(wybor>7) {
			System.out.println("Koncze program!");
		}
	}
}
