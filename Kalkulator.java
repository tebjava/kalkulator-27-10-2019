import java.util.Scanner;

public class Kalkulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Kalkulator w Java");
		System.out.println("Wybierz jedna z operacji:");
		System.out.println("1. Dodawanie\n2. Odejmowanie"
				+ "\n3. Mnożenie\n4. Dzielenie\n5. Wyjscie");
		System.out.print("Wybor: ");
		Scanner skan=new Scanner(System.in);
		int wybor=skan.nextInt();
		
		//Ponizsza linia wskazuje, ze program pobral wartosc od uzytkownika
		//nie jest potrzebna w programie wiec zostala zakomentowana
		//System.out.println("Wybrales opecje numer " + wybor);
		
		/*Instrukcje warunkowe
		 * Programy komputerowe używaja instrukcji warunkowych
		 * w celu reagowania na dane podawane przez uztywkonika
		 * lub poprzez inne programy czy tez w celu reakcji
		 * na wlasne dzialania (np. sumowanie, odejmowanie, 
		 * losowanie itp.)
		 * 
		 * Warunki wykonuja sie TYLKO WTEDY, kiedy ich wynikiem
		 * jest wartosc logiczna PRAWDA (true). Wyrazenia warunkow
		 * sa zaczerpiete bezposrednio z analizy matematycznej
		 * i przedstawiaja sie nastepujaco:
		 * a>b - warunek jest spelniony gdy wartosc a jest wieksza od b (np. 5>3)
		 * a>=b - jak powyzej, lecz domkniecie przedzialu nieostre (a może byc ROWNE badz wieksze, np. 5>=5, 5>=4)
		 * a<b - warunek jest spelniony gdy wartosc a jest mniejsza od b (np. 5<10)
		 * a<=b - jak powyzej, lecz domkniecie przedzialu nieostre (a moze byc ROWNE badz mniejsze np. 5<=5, 5<6)
		 * a==b - warunek jest spelniony gdy obie wartosci sa takie same (np. 10==10)
		 * a!=b - warunek jest spelniony gdy wartosc a jest rozna od be (np. 10!=15)
		 * 
		 * Powyzsze wyrazenia podaje sie w specjalnych instrukcjach warunkowych
		 * zwanych if(). Przyklad kodu:
		 * Scanner skaner = new Scanner(System.in);
		 * int a= skaner.nextInt();
		 * if(a>10) {
		 *      //ten kod wykona sie tylko wtedy, gdy uzytkownik poda wartosc wieksza od 10
		 * 		System.out.print("Podales wartosc wieksza od 10!");
		 * }
		 * 
		 * Innym wyrazeniem jest if() {} else {}. Pozwala
		 * na dodanie kodu, ktory wykona sie w przypadku
		 * gdy warunek z if nie zostanie spelniony. przyklad:
		 * Scanner skaner = new Scanner(System.in);
		 * int a= skaner.nextInt();
		 * if(a>10) {
		 *      //ten kod wykona sie tylko wtedy, gdy uzytkownik poda wartosc wieksza od 10
		 * 		System.out.print("Podales wartosc wieksza od 10!");
		 * }
		 * else {
		 * 		System.out.print("Podales wartosc mniejsza badz rowna 10!");
		 * }
		 * 
		 * Ostatni rodzaj warunkow to if() {} else if() {} (... else if() {}...) else {}
		 * 
		 * Pozwala on na przepatrywanie wiecej niz jednego warunkow (dowolna ilosc - wedle
		 * potrzeb programisty). Warunki czytane sa jeden po drugim totez
		 * programista powinien okreslic ich priorytet. Przyklad
		 * 
		 * Scanner skaner = new Scanner(System.in);
		 * int a= skaner.nextInt();
		 * if(a>10) {
		 *      //ten kod wykona sie tylko wtedy, gdy uzytkownik poda wartosc wieksza od 10
		 * 		System.out.print("Podales wartosc wieksza od 10!");
		 * }
		 * else if (a==10) {
		 * 		System.out.print("Podales wartosc rowna 10!");
		 * }
		 * else {
		 * 		System.out.print("Podales wartosc mniejsza od 10!");
		 * }
		 * 
		 * Przyklad niepoprwanego ustawienia warunkow:
		 * 
		 * Scanner skaner = new Scanner(System.in);
		 * int a= skaner.nextInt();
		 * if(a>=10) {
		 *      //warunek wykona sie do a wiekszego lub rownego 10
		 * 		System.out.print("Podales wartosc wieksza od 10!");
		 * }
		 * else if (a==10) {
		 *		//dlatego ten warunek w ogole nie zostanie wykonany!
		 *		//naprawa tego warunku sprowadza sie do przestawienia 
		 * 		//miejscami warunkow (albo zmiana warunku pierwszego)
		 * 		System.out.print("Podales wartosc rowna 10!");
		 * }
		 * else {
		 * 		System.out.print("Podales wartosc mniejsza od 10!");
		 * } 
		 * 
		 * Proponowana naprawa:
		 * Scanner skaner = new Scanner(System.in);
		 * int a= skaner.nextInt();
		 * if (a==10) {
		 *      //gdy a rowne 10 wykona sie ten kod
		 * 		System.out.print("Podales wartosc rowna 10!");
		 * }		 * 
		 * else if(a>=10) {
		 * 	    //pomimo akceptacji a==10 dla a rownego 10 
		 *		//kod nie wykona sie bo wykona sie pierwszy warunek w kolejce
		 *		//(czyli a==10) a ten kod wykona sie gdy a bedzie wieksze od 10
		 * 		System.out.print("Podales wartosc wieksza od 10!");
		 * }
		 * else {
		 * 		System.out.print("Podales wartosc mniejsza od 10!");
		 * } 
		 */
		
		if(wybor==1) {
			//System.out.println("Wybrales dodawanie!");
			System.out.print("Podaj wartosc a: ");
			int a = skan.nextInt();
			System.out.print("Podaj wartosc b: ");
			int b = skan.nextInt();
			//int c=a+b;
			System.out.println(a + " + " + b + " = " + (a+b));
		}
		
		if(wybor==2) {
			System.out.println("Wybrales odejmowanie!");
		}
		
		if(wybor==3) {
			System.out.println("Wybrales mnozenie!");
		}
		
		if(wybor==4) {
			System.out.println("Wybrales dzielenie!");
		}
		
		if(wybor>4) {
			System.out.println("Koncze program!");
		}
	}

}
